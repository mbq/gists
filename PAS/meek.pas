{
 Extracted from:
 Algorithm 123 — SINGLE TRANSFERABLE VOTE BY MEEK’S METHOD
 I. D. HILL, B. A. WICHMANN and D. R. WOODALL
 Adapted slightly to be compilable by Free Pascal Compiler
}

{$mode iso}
Program stvpas (datafile, output);


{
  This program counts the votes in a Single Transferable Vote election,
    using Meek 's method, and reports the results}

{If there are more than 40 candidates an increase in the size of
MaxCandidates is the only change needed}

Const MaxCandidates = 40;
  NameLength = 20;


Type Candidates = 1 .. MaxCandidates;
  CandRange = 0 .. MaxCandidates;
  name = PACKED ARRAY [1 .. NameLength] Of char;


Var NumCandidates, NumSeats: Candidates;
  candidate, NumElected, NumExcluded,
  multiplier, ignored: CandRange;
  Droop, excess, quota, total: real;
  faulty, SomeoneElected, RandomUsed: Boolean;
  FracDigits: 1 .. 4;
  table, seed1, seed2, seed3: integer;
  datafile: TextFile;
  title: name;
  votes, weight: ARRAY [Candidates] Of real;
  status: ARRAY [Candidates] Of (Hopeful, Elected, NewlyElected,
                                 Almost, Excluded, ToBeExcluded, NotUsed, Used);
  names: ARRAY [Candidates] Of name;



Function InInteger: integer;
{Reads the next integer from datafile and returns its value}

Var i: integer;
Begin
  read(datafile, i);
  InInteger := i
End; {InInteger}
Procedure PrintOut;
{Updates the table number and prints out the current results}

Var arg: real;
  cand: Candidates;
Begin
  table := table + 1;
  writeln;
  writeln(' ': 20, title);
  writeln;
  write(' Table:', table: 1);
  writeln(' Quota:', quota: 1: FracDigits);
  writeln;
{The numbers of blanks following Candidate, Retain and
Transfer are 12, 3 and 3 respectively}
  writeln(' Candidate Retain Transfer Votes ');
  writeln;
  For cand := 1 To NumCandidates Do
    Begin
      write(names[cand]);
      If status[cand] = ToBeExcluded Then
        arg := 100.0
      Else arg := 100.0 * weight[cand];
      write(arg: 6: 1, ' % ');
      write(100.0 - arg: 8: 1, ' % ');

{If it is valid to do so, print quota instead of votes[cand]
because the latter might have a small rounding error that
would confuse unsophisticated users}
      If status[cand] = Elected Then arg := votes[cand] / quota
      Else arg := 0.0;
      If (arg >= 0.99999) And (arg <= 1.00001) Then arg := quota
      Else arg := votes[cand];
      write(arg: 10: FracDigits, ' ');
      If status[cand] = Excluded Then write(' Excluded ')
      Else If status[cand] = Elected Then write(' Elected ')
      Else If status[cand] = NewlyElected Then write(' Newly Elected ')
      Else If status[cand] = ToBeExcluded Then
             Begin
               write(' To be Excluded ');
               status[cand] := Excluded
             End;
      writeln;
      If (NumCandidates > 9) And (cand Mod 5 = 0) And
         (cand <> NumCandidates) Then writeln
    End;
  writeln;
  writeln(' Excess ', excess: 40: FracDigits);
  writeln;
  writeln(' Total ', total: 40: FracDigits);
  writeln;
  writeln
End; {PrintOut}
Procedure elect(cand: Candidates);
Begin
  status[cand] := NewlyElected;
  NumElected := NumElected + 1
End; {elect}
Procedure exclude(cand: Candidates);
Begin
  status[cand] := ToBeExcluded;
  weight[cand] := 0.0;
  NumExcluded := NumExcluded + 1;
  If RandomUsed Then
    Begin
      writeln;
      writeln;
      writeln(' Random choice used to exclude ', names[cand])
    End
End; {exclude}
Function LowestCandidate: CandRange;

{Returns the candidate number of the candidate who currently has the
lowest number of votes. If two or more are equal lowest, then a
pseudo-random choice is made between them}

Var cand: Candidates;
  LowCand: CandRange;
Function random: real;

{Returns a pseudo-random number rectangularly distributed
between 0 and 1. Based on Wichmann and Hill, Algorithm
AS 183, Appl. Statist. (1982) 31, 188 - 190}

Var rndm: real;
Begin
{ If seeds have not been set, then set them}
  If seed1 = 0 Then
    Begin
      seed1 := NumCandidates;
      seed2 := NumSeats + 10000;
      rndm := total + 20000.0;
      While rndm > 30322.5 Do
        rndm := rndm - 30322.0;
      seed3 := round(rndm)
    End;
  seed1 := 171 * (seed1 Mod 177) - 2 * (seed1 Div 177);
  seed2 := 172 * (seed2 Mod 176) - 35 * (seed2 Div 176);
  seed3 := 170 * (seed3 Mod 178) - 63 * (seed3 Div 178);
  If seed1 < 0 Then seed1 := seed1 + 30269;
  If seed2 < 0 Then seed2 := seed2 + 30307;
  If seed3 < 0 Then seed3 := seed3 + 30323;
  rndm := seed1 / 30269.0 + seed2 / 30307.0 + seed3 / 30323.0;
  random := rndm - trunc(rndm)
End; {random}
Function lower(cand, lowest: CandRange): Boolean;

{Find whether cand has fewer votes than lowest, and also
reports whether a random choice had to be made}

Var lowly: Boolean;
Begin
  If lowest = 0 Then
    Begin
      RandomUsed := false;
      lower := true
    End
  Else If votes[cand] = votes[lowest] Then
         Begin
           RandomUsed := true;

{Multiplier is used to make all equally-lowest candidates
equally likely to be chosen, even though they are
considered serially and not simultaneously}
           lower := (multiplier * random < 1.0)
         End
  Else
    Begin
      lowly := (votes[cand] < votes[lowest]);
      lower := lowly;
      If lowly Then RandomUsed := false
    End;
  If RandomUsed Then multiplier := multiplier + 1
  Else multiplier := 2
End; {lower}
Begin
  LowCand := 0;
  For cand := 1 To NumCandidates Do
    If (status[cand] = Hopeful) Or (status[cand] = Almost) Then
      If lower(cand, LowCand) Then LowCand := cand;
  LowestCandidate := LowCand
End; {LowestCandidate}
Procedure compute;

{This is the heart of the program, which counts the votes, taking
the current weights into account, and adjusts the weights and
the quota iteratively to attain the required solution}

{MaxIterations is the maximum number of iterations allowed in
calculating the weights. It is unlikely that so many will
ever be used, but its value may be increased if desired}

Const MaxIterations = 500;

Var temp, value: real;
  count, iteration: integer;
  cand: CandRange;
  converged, ended: Boolean;
Procedure Rewind;

{Returns to the beginning of datafile, and ignores the first two
numbers on it. These are the number of candidates and the
number of seats, whose values are not needed again. Numbers
indicating withdrawn candidates are also ignored}

Var ig, ignore: integer;
Begin
  reset (datafile);
  For ig := -1 To ignored Do
    ignore := InInteger
End; {Rewind}
Begin
  iteration := 1;
  Repeat
    Rewind;
    excess := 0.0;
    For cand := 1 To NumCandidates Do
      votes[cand] := 0.0;
    count := InInteger;
    While count > 0 Do
      Begin
        value := count;
        cand := InInteger;
        ended := false;
        While cand>0 Do
          Begin
            If Not ended And (weight[cand] > 0.0) Then
              Begin
                ended := (status[cand] = Hopeful);
                If ended Then
                  Begin
                    votes[cand] := votes[cand] + value;
                    value := 0.0
                  End
                Else
                  Begin
                    votes[cand] := votes[cand] + value * weight[cand];
                    value := value * (1.0 - weight[cand])
                  End
              End;
            cand := InInteger
          End;
        excess := excess + value;
        count := InInteger
      End;
    quota := (total - excess) * Droop;

{The next statement is unlikely ever to be used, but is a
safeguard against certain pathological test data}
    If quota < 0.0001 Then quota := 0.0001;
    converged := true;
    For cand := 1 To NumCandidates Do
      If status[cand] = Elected Then
        Begin
          temp := quota / votes[cand];
          If (temp > 1.00001) Or (temp < 0.99999) Then
            converged := false;
          temp := weight[cand] * temp;
          weight[cand] := temp;

{The next statement is unlikely ever to be used, but is
a safeguard against certain pathological test data}
          If temp > 1.0 Then weight[cand] := 1.0
        End;
    iteration := iteration + 1
  Until (iteration = MaxIterations) Or converged;
  If Not converged Then
    Begin

{The "Failure to converge" message is unlikely ever to appear.
If it does, increasing MaxIterations will probably cure it}
      writeln;
      writeln;
      writeln(' Failure to converge ');
      writeln
    End;
  count := 0;
  For cand := 1 To NumCandidates Do
    If (status[cand] = Hopeful) And (votes[cand] >= quota) Then
      Begin
        status[cand] := Almost;
        count := count + 1
      End;

{Allow for the special case where there is a multi-way tie and
too many candidates reach the quota simultaneously}
  While NumElected + count > NumSeats Do
    Begin
      PrintOut;
      RandomUsed := false;
      For cand := 1 To NumCandidates Do
        If status[cand] = Hopeful Then exclude(cand);
      exclude(LowestCandidate);
      count := count - 1
    End;
  SomeoneElected := false;
  For cand := 1 To NumCandidates Do
    If status[cand] = Almost Then
      Begin
        elect(cand);
        SomeoneElected := true
      End;
  If SomeoneElected Then PrintOut;
  For cand := 1 To NumCandidates Do
    If status[cand] = NewlyElected Then
      Begin
        If NumElected < NumSeats Then
          weight[cand] := quota / votes[cand];
        status[cand] := Elected
      End
End; {compute}
Procedure complete;

{Used to elect all remaining candidates if the number
remaining equals the number of seats remaining}

Var cand: Candidates;
Begin
  For cand := 1 To NumCandidates Do
    If status[cand] = Hopeful Then elect(cand)
End; {complete}
Procedure Preliminaries;
{Checks datafile for errors and sets initial values of variables}

Var cand, count, LineNo: integer;
Procedure error(cand: integer; TooBig: Boolean);
Begin
  writeln;
  write (' On line ' , LineNo: 1, ', Candidate ', cand: 1);
  If TooBig Then write (' exceeds maximum ')
  Else write (' is repeated ');
  writeln;
  faulty := true
End; {error}
Procedure ReadName(Var n: name);

{Reads the name of a candidate, or reads a title, and stores
it for later use. If the name has more than NameLength
characters the excess ones will be disregarded. If it
has fewer than NameLength characters blanks will be used
to extend it}

Var i: integer;
  ch: char;
Begin
  Repeat
    read(datafile, ch)
  Until ch = '"';
  i := 0;
  read(datafile, ch);
  While ch <> '"' Do
    Begin
      If i < NameLength Then
        Begin
          i := i + 1;
          n[i] := ch
        End;
      read(datafile, ch)
    End;
  While i < NameLength Do
    Begin
      i := i + 1;
      n[i] := ' '
    End
End; {ReadName}
Begin
  Droop := 1.0/(NumSeats + 1);
  LineNo := 1;
  seed1 := 0;
  total := 0.0;
  table := 0;
  NumElected := 0;
  NumExcluded := 0;
  ignored := 0;
  For cand := 1 To NumCandidates Do
    weight[cand] := 1.0;
  count := InInteger;
{Deal with withdrawals, if any}
  While count < 0 Do
    Begin
      weight[-count] := 0.0;
      count := InInteger
    End;
  While count > 0 Do
    Begin
      LineNo := LineNo + 1;
      total := total + count;
      For cand := 1 To NumCandidates Do
        status[cand] := NotUsed;
      cand := InInteger;
      While cand > 0 Do
        Begin
          If cand > NumCandidates Then error(cand, true)
          Else If status[cand] = Used Then error(cand, false)
          Else status[cand] := Used;
          cand := InInteger
        End;
      count := InInteger
    End;
  For cand := 1 To NumCandidates Do
    Begin
      ReadName(names[cand]);
      status[cand] := Hopeful;
      If weight[cand] < 0.5 Then
        Begin
          status[cand] := Excluded;
          NumExcluded := NumExcluded + 1;
          ignored := ignored + 1
        End
    End;
  ReadName(title);
  If Not faulty Then
    Begin

{FracDigits controls the number of digits beyond the decimal
point that will be printed in the output tables}
      FracDigits := 4;
      If total > 999.5 Then FracDigits := FracDigits - 1;
      If total > 99.5 Then FracDigits := FracDigits - 1;
      If total > 9.5 Then FracDigits := FracDigits - 1
    End
End; {Preliminaries}
{Start of main program}
Begin
  reset(datafile);
  NumCandidates := InInteger;
  NumSeats := InInteger;
  writeln;
  writeln;
  writeln(' Number of Candidates = ', NumCandidates: 1);
  writeln (' Number of seats = ', NumSeats: 1);
  If NumCandidates < NumSeats Then writeln(' All candidates elected ')
  Else
    Begin
      faulty := false;
      Preliminaries;
      If NumCandidates <= NumSeats + NumExcluded Then
        writeln(' All non - withdrawn candidates elected ')
      Else
        Begin
{The Preliminaries procedure will have reset faulty to true if
the data contain errors}
          If Not faulty Then
            Begin
              Repeat

{Count votes and elect candidates, transferring
surpluses until no more can be done or all
seats are filled}
                Repeat
                  compute
                Until Not SomeoneElected Or (NumElected >= NumSeats);
{Unless the election is finished, someone must
now be excluded}
                If NumElected < Numseats Then
                  Begin
                    PrintOut;
                    exclude(LowestCandidate);
                    If NumCandidates - NumExcluded = NumSeats
                      Then complete
                    Else PrintOut
                  End
              Until NumElected = NumSeats;

{Now that all seats are filled, exclude any candidates not
already elected, and print out the final table}
              RandomUsed := false;
              For candidate := 1 To NumCandidates Do
                If status[candidate] = Hopeful Then exclude(candidate);
              PrintOut;
            End
        End
    End
End.
