fn fixed_iter<F: Fn(S) -> S, S: Copy>(f: F, s0: S) -> impl Iterator<Item = S> {
    std::iter::successors(Some(s0), move |&x| Some(f(x)))
}

fn cycle_length<F: Fn(S) -> S + Copy, S: Copy + Eq>(f: F, s0: S) -> usize {
    let tortoise = fixed_iter(f, s0).skip(1);
    let hare = fixed_iter(move |x| f(f(x)), s0).skip(1);
    let (cross, _) = tortoise.zip(hare).find(|(a, b)| a == b).unwrap();

    let lam = fixed_iter(f, cross)
        .skip(1)
        .take_while(|&x| x != cross)
        .count();
    lam + 1
}

fn main() {
    let f = |x| (x + 7) % 311;
    //let f = |_x| 7;

    let a = cycle_length(f, 0);
    println!("{}", a);

    let fib = |(a, b)| (b, (a + b) % 10);

    let a = cycle_length(fib, (1, 1));
    println!("{}", a);

    let f = |x| if x < 10 { x + 1 } else { 10 + (x + 3) % 5 };
    let a = cycle_length(f, 0);
    println!("{}", a);
}
