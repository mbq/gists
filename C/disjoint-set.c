#include <stdlib.h>
#include <stdio.h>
#include <string.h>

//This is a Disjoint-set data structure demo, more here:
//https://en.wikipedia.org/wiki/Disjoint-set_data_structure

//Input:
//N\n number of objects
//a b\n a, b are object ids (starts from one), and the line means they are connected
//a2 b2\n etc. times number of all edges

//Output:
//:: x belongs to cy\n, x is object id, y is the (1-based) index of a cluster x belongs to

int find(int idx,int *where){
 return (where[idx-1]<0)?idx:(where[idx-1]=find(where[idx-1],where));
}

int getVal(int idx,int *where){
 return (idx<0)?idx:(where[idx-1]=getVal(where[idx-1],where));
}

int main(int argc,char **argv){
 int N;
 scanf("%i",&N);

 //Q[i]>0 1-based index in Q
 //Q[i]==0 NULL
 //Q[i]<0 tree rank / cluster id
 int *Q=malloc(sizeof(int)*N);
 memset(Q,0,N);
 int a,b;
 while(scanf("%i %i",&a,&b)==2){
  if(a<=0 || a>N || b<=0 || b>N){
   printf("Error, bad input!\n");
   free(Q);
   return(-1);
  }
  //Add elements if they were not considered earlier
  if(!Q[a-1]) Q[a-1]=-1;
  if(!Q[b-1]) Q[b-1]=-1;

  //Ok, merge two trees
  int ra=find(a,Q);
  int rb=find(b,Q);
  if(ra==rb) continue; //Cool, both are already in the same cluster
  if(Q[ra-1]<Q[rb-1]){
   //Rank a is bigger, b to a
   Q[rb-1]=ra;
  }else{
   //Rank b is bigger, a to b
   //... but first, we increase the rank of ra if both trees have equal rank
   if(Q[ra-1]==Q[rb-1]) Q[rb-1]--;
   Q[ra-1]=rb;
  }
 }

 //Throw away ranks, fill with cluster IDs
 int cid=1;
 for(int e=0;e<N;e++) if(Q[e]<0) Q[e]=-(cid++);

 //Construct answer
 for(int e=0;e<N;e++){
  //Ignore paths to nowhere (orphan, single nodes)
  if(!Q[e]) continue;
  //Collapse path Q[e]
  getVal(e+1,Q);
  printf(":: %d belongs to c%d\n",e+1,-Q[e]);
 }

 printf("DONE\n");
 free(Q);
 return(0);
}
