#include <stdio.h>
#include <omp.h>

int main(int argc,char **argv){
 //omp_set_num_threads(XXX);
 int maxt=omp_get_max_threads();
 printf("Max threads: %d\n",maxt);
 int z[maxt];
 for(int e=0;e<maxt;e++) z[e]=0;
 #pragma omp parallel for
 for(int e=0;e<50;e++){
  z[omp_get_thread_num()]++;
  printf("%d makes %d\n",omp_get_thread_num()+1,e+1);
 }
 for(int e=0;e<maxt;e++)
  printf("thread %d made %d\n",e+1,z[e]);
 return 0;
}
